<?php
Namespace dgifford\Traits;

/*
	Methods for setting properties.

    Copyright (C) 2016  Dan Gifford

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */






trait PropertySetterTrait
{
	/**
	 * Returns an array of the public properties in this class.
	 * Exposes the private getProperties method.
	 * 
	 * @return array
	 */
	public function getPublicProperties()
	{
		return $this->getProperties( 'IS_PUBLIC' );
	}



	/**
	 * Returns an array of the protected properties in this class.
	 * Exposes the private getProperties method.
	 * 
	 * @return array
	 */
	public function getProtectedProperties()
	{
		return $this->getProperties( 'IS_PROTECTED' );
	}



	/**
	 * Returns an array of the static properties in this class.
	 * Exposes the private getProperties method.
	 * 
	 * @return array
	 */
	public function getStaticProperties()
	{
		return $this->getProperties( 'IS_STATIC' );
	}



	/**
	 * Returns an array of the private properties in this class.
	 * Exposes the private getProperties method.
	 * 
	 * @return array
	 */
	public function getPrivateProperties()
	{
		return $this->getProperties( 'IS_PRIVATE' );
	}



	/**
	 * Returns an array of *all* properties in this class.
	 * Exposes the private getProperties method.
	 * 
	 * @return array
	 */
	public function getAllProperties()
	{
		return $this->getProperties();
	}



	/**
	 * Returns an array of property names matching $type
	 * using reflection.
	 * 
	 * @param  string $type
	 * @return array
	 */
	private function getProperties( $type = '' )
	{
		$result = [];
		$properties = [];

		// Reflect this class
		$reflected = new \ReflectionClass( $this );

		$type_constants = [
			'static' 	=> 'IS_STATIC',
			'public' 	=> 'IS_PUBLIC',
			'protected' => 'IS_PROTECTED',
			'private' 	=> 'IS_PRIVATE',
		];

		if( isset( $type_constants[ $type ] ) )
		{
			$type = $type_constants[ $type ];
		}

		if( in_array( $type, $type_constants ) )
		{
			$properties = $reflected->getProperties( constant( '\ReflectionProperty::' . $type ) );
		}
		else
		{
			foreach( $type_constants as $type )
			{
				if( $type != 'IS_STATIC' )
				{
					$properties = array_merge( $properties, $reflected->getProperties( constant( '\ReflectionProperty::' . $type ) ) );
				}
			}
		}
		
		foreach( $properties as $property )
		{
			$result[] = $property->getName();
		}

		return $result;
	}



	/**
	 * Overwrite public properties with arguments passed in array.
	 * If force_type is true, public properties will only be used if
	 * they are the same type as the defaults, except if the default 
	 * is NULL, in which case it will be overwritten by the public 
	 * value regardless.
	 * 
	 * @param array $p
	 */
 	public function setPublicProperties( array $p = [], string $method_prefix = null, bool $camel_case = false )
	{
		// Reflect this class
		$reflected = new \ReflectionClass( $this );

		// Get default values
		$defaults =  $reflected->getDefaultProperties();

		// Get all public properties
		$public_properties = $this->getPublicProperties();

		// Loop through passed properties
		foreach( $p as $property => $value )
		{
			// Check it is a public property
			if( in_array( $property, $public_properties ) )
			{
				// Use setter methods
				if( is_string( $method_prefix ) )
				{
					$method = $this->getPropertySetterMethodName( $property, $method_prefix, $camel_case );

					if( is_callable([ $this, $method]) )
					{
						$this->$method( $value );
					}
					else
					{
						throw new \BadMethodCallException( "Method is not callable: `{$method}`" );	
					}
				}
				// Simple validation by variable type
				else
				{
					// Check the type
					if( 
						( gettype($value) === gettype($defaults[$property]) )
						or
						( is_null($defaults[$property]) )
					)
					{
						$this->$property = $value;
					}
					else
					{
						throw new \InvalidArgumentException( "Attempting to set public property `{$property}` with incorrect type" );
					}
				}	
			}
		}
	}


	/**
	 * Return the name of a setter method.
	 * 
	 * @param  string   $property
	 * @param  string   $prefix
	 * @param  bool  	$camel_case
	 * @return string
	 */
	protected function getPropertySetterMethodName( string $property, string $prefix = 'set_', bool $camel_case = false )
	{
		if( $camel_case )
		{
			return implode( '', array_merge([$prefix], array_map( 'ucfirst', explode('_', $property) )) );
		}
		
		return $prefix . $property;
	}



	/**
	 * Retrieves all properties in a class and calls a setter
	 * method (if it exists) with each property value.
	 *  
	 * @param  string  	$method_prefix
	 * @param  bool  	$camel_case
	 * @return void
	 */
	public function callAllSetters( $method_prefix = 'set_', bool $camel_case = false )
	{
		// Get all properties
		$properties = $this->getAllProperties();

		foreach( $properties as $property )
		{
			$method = $this->getPropertySetterMethodName( $property, $method_prefix, $camel_case );

			if( is_callable([ $this, $method]) )
			{
				$this->$method( $this->$property );
			}
		}
	}
}