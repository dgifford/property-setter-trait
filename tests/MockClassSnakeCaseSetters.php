<?php
Namespace dgifford\Traits\Tests;



class MockClassSnakeCaseSetters
{
	Use \dgifford\Traits\PropertySetterTrait;



	public $string = '';

	public $array = [];

	public $bool = false;

	public $anything;

	protected $cannot_be_set;



	public function __construct( $properties = [] )
	{
		/*
			Set public properties from $properties array.
			Only set properties if they are the same type 	
			by setting the second argument to true.
			$anything can be set to any type because it is null.
		*/
		$this->setPublicProperties( $properties );
	}



	public function set( $properties = [] )
	{
		/*
			Set public properties from $properties array.
			Use 'set_' methods
		*/
		$this->setPublicProperties( $properties, 'set_' );
	}



	public function get_cannot_be_set()
	{
		return $this->cannot_be_set;
	}



	public function set_cannot_be_set()
	{
		$this->cannot_be_set = 'cannot_be_set';
	}



	public function set_anything( $value = null )
	{
		if( is_null( $value ) )
		{
			$this->anything = 'A default value';
		}
	}



	public function set_string( $value = '' )
	{
		if( is_string( $value ) )
		{
			$this->string = $value;
		}
		else
		{
			$this->string = 'string';
		}
	}
}

