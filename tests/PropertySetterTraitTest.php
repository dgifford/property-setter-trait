<?php
Namespace dgifford\Traits\Tests;



/**
 * Auto Loader
 * 
 */
require_once(__DIR__ . '/../vendor/autoload.php');



class PropertySetterTraitTest extends \PHPUnit\Framework\TestCase
{
	public function setUp(): void
	{
		$this->mock_class = new MockClassSnakeCaseSetters([
			'string' 		=> 'Hello world',
			'array' 		=> ['Hello world'],
			'bool' 			=> false,
			'anything' 		=> 'Hello world',
			'cannot_be_set'	=> 'wibble',
		]);
	}



	public function testCanSetNullProperty()
	{
		$this->assertSame( 'Hello world', $this->mock_class->anything );
	}



	public function testPropertyTypeMaintained()
	{
		$this->assertFalse( $this->mock_class->bool );
	}



	public function testCannotSetProtectedProperty()
	{
		$this->assertNull( $this->mock_class->get_cannot_be_set() );
	}



	public function testCallAllSetters()
	{
		$this->mock_class->callAllSetters();

		// $this->mock_class->set_anything uses the value passed when the object was instantiated
		$this->assertSame( 'Hello world', $this->mock_class->anything );

		// $this->mock_class->set_cannot_be_set always sets a default values
		$this->assertSame( 'cannot_be_set', $this->mock_class->get_cannot_be_set() );
	}



	public function testUseSnakeCaseSetMethods()
	{
		$mock_class = new MockClassSnakeCaseSetters();

		$this->assertSame( '', $mock_class->string );

		$mock_class->set(['string' => false]);

		$this->assertSame( 'string', $mock_class->string );

		$mock_class->set(['string' => 'foo']);

		$this->assertSame( 'foo', $mock_class->string );
	}



	public function testUseCamelCaseSetMethods()
	{
		$mock_class = new MockClassCamelCaseSetters();

		$this->assertSame( '', $mock_class->string );

		$mock_class->set(['string' => false]);

		$this->assertSame( 'string', $mock_class->string );

		$mock_class->set(['string' => 'foo']);

		$this->assertSame( 'foo', $mock_class->string );
	}



	public function testSetMethodDoesNotExist()
	{
		$this->expectException( \BadMethodCallException::class );

		$mock_class = new MockClassSnakeCaseSetters();

		$mock_class->set(['string' => 'foo', 'array' => [1] ]);
	}

}