# Property Setter Trait

Provides a method to set properties of a class with an array.

```
class Foo
{
	Use \dgifford\Traits\PropertySetterTrait;



	public $string = '';

	public $array = [];

	public $bool = false;

	public $anything;

	protected $cannot_be_set;



	public function __construct( $properties = [] )
	{
		/*
			Set public properties from $properties array.
			Only sets properties if they are the same type.
			$anything can be set to any type because it is null.
		*/
		$this->setPublicProperties( $properties, true );
	}
}


$foo = new Foo([

	'string' 	=> 'Hello world',
	'array' 	=> ['Hello world'],
	'bool' 		=> false,
	'anything' 	=> 'Hello world',

]);

echo Foo->string; // 'Hello world'

```

The value in the array will only be used if it is of the same type as the default property in the class.

## Automatically calling setter methods

```setPublicProperties()``` can call a method to set each property in the array instead of matching property type. 

```setPublicProperties()``` accepts a string prefix as the second argument which is used to call a setter method with the value. By default, the method name is generated in the format ```$prefix . $property_name```. Adding a third boolean true parameter will convert the ```$property_name``` into camel case.

If a method doesn't exist for setting the property, a ```BadMethodCallException``` is thrown.

```
class Foo
{
	Use \dgifford\Traits\PropertySetterTrait;



	public $string = '';

	public $array = [];

	public $bool = false;

	public $anything;

	protected $cannot_be_set;



	public function __construct( $properties = [] )
	{
		/*
			Set public properties from $properties array.
			Only set properties if they are the same type 	
			by setting the second argument to true.
			$anything can be set to any type because it is null.
		*/
		$this->setPublicProperties( $properties, 'set_' );
	}



	/**
	 * Method for setting the $string property.
	 */
	public function set_string( $value = '' )
	{
		if( is_string( $value ) )
		{
			this->string = $value;
		}
	}
}


$foo = new Foo([
	'string' 	=> false,
]);

echo Foo->string; // ''

$foo = new Foo([
	'string' 	=> 'Hello world',
]);

echo Foo->string; // 'Hello world'

```
## Calling all setter methods

The ```callAllSetters()``` method retreives all properties in a class and passes them to any setter methods that exist.

This can be used for more complex manipulation of properties (e.g. after using ```setPublicProperties()``` )

```
class Foo
{
	Use dgifford\Traits\PropertySetterTrait;



	public $anything;

	protected $protected;



	public function set_anything()
	{
		$this->anything = 'anything';
	}
	


	public function set_protected()
	{
		$this->protected = 'protected';
	}



	public function getProtected()
	{
		return $this->protected;
	}
}


$foo = new Foo;

$foo->callAllSetters();

echo $foo->anything; // 'anything'
echo $foo->getProtected(); // 'protected'
```